from django.db import models


# create your models here ---- THIS IS THE APP DIRECTORY FILE.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(
        max_length=100
    )  # this will determine how long the name of your task is
    due_date = models.DateTimeField(
        blank=True, null=True
    )  # This will define time for your tasks
    is_completed = models.BooleanField(
        default=False
    )  # boolean is fun to say. booooolean.
    list = models.ForeignKey(
        "TodoList", on_delete=models.CASCADE, related_name="items"
    )
