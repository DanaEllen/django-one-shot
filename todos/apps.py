from django.apps import AppConfig


# STILL IN APP DIRECTORY WHEN YOU ARE TYPING IN HERE SO PAY ATTENTION-
class TodosConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "todos"
