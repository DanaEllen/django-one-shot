from django.contrib import admin
from .models import TodoList

# Register your models here, you big dummy. and this time remember to @the admin


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
